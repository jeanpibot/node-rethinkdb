const products = [
    {
        id: 1,
        name: 'Computer 1',
        price: 1200,
        desc: 'blablablabalblablablabla',
    },
    {
        id: 2,
        name: 'Computer 2',
        price: 2200,
        desc: 'blablablabalblablablabla',
    },
    {
        id: 3,
        name: 'Computer 3',
        price: 900,
        desc: 'blablablabalblablablabla',
    },
    {
        id: 4,
        name: 'Computer 4',
        price: 2500,
        desc: 'blablablabalblablablabla',
    },
];

const people = [
    {
        id: 1,
        name: 'Luis Alejandro Ramirez',
        age: 12,
    },
    {
        id: 2,
        name: 'Maria Alejandra Ortiz',
        age: 22,
    },
    {
        id: 3,
        name: 'Juan Pedro Merchan',
        age: 19,
    },
    {
        id: 4,
        name: 'Jean Pierre Giovanni Arenas Ortiz',
        age: 29,
    },
];

module.exports = { products, people };
